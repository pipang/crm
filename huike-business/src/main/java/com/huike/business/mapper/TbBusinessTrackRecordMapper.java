package com.huike.business.mapper;

import java.util.List;
import com.huike.business.domain.TbBusinessTrackRecord;
import com.huike.business.domain.vo.BusinessTrackVo;
import com.huike.clues.domain.TbClueTrackRecord;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商机跟进记录Mapper接口
 * @date 2021-04-28
 */
@Mapper
public interface TbBusinessTrackRecordMapper {

    /**
     * 获取商机跟进记录
     * @param id
     * @return
     */
    List<TbBusinessTrackRecord> getBusinessRecordList(Long id);


    /**
     * 线索更进表新增数据
     * @param tbBusinessTrackRecord
     */
    void insert(TbBusinessTrackRecord tbBusinessTrackRecord);
}