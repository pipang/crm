package com.huike.business.service.impl;


import com.huike.business.domain.TbBusiness;
import com.huike.business.domain.TbBusinessTrackRecord;
import com.huike.business.domain.vo.BusinessTrackVo;
import com.huike.business.mapper.TbBusinessMapper;
import com.huike.business.mapper.TbBusinessTrackRecordMapper;
import com.huike.business.service.ITbBusinessTrackRecordService;
import com.huike.clues.domain.TbClue;
import com.huike.clues.domain.TbClueTrackRecord;
import com.huike.common.utils.SecurityUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * 商机跟进记录Service业务层处理
 * 
 * @author wgl
 * @date 2021-04-28
 */
@Service
public class TbBusinessTrackRecordServiceImpl implements ITbBusinessTrackRecordService {

    @Autowired
    private TbBusinessTrackRecordMapper tbBusinessTrackRecordMapper;
    @Autowired
    private TbBusinessMapper tbBusinessMapper;

    /**
     * 查询商机跟进记录
     * @param id
     * @return
     */
    @Override
    public List<TbBusinessTrackRecord> getBusinessTrackRecordList(Long id) {
        List<TbBusinessTrackRecord> businessRecordList = tbBusinessTrackRecordMapper.getBusinessRecordList(id);
        return businessRecordList;
    }

    /**
     * 新增商机跟进记录
     * @param businessTrackVo
     */
    @Override
    public void add(BusinessTrackVo businessTrackVo) {
        // 2.1 由于查询需要用到用户名 调用工具类获取用户名
        String username = SecurityUtils.getUsername();
        //设置线索表的属性
        TbBusiness tbBusiness = new TbBusiness();
        BeanUtils.copyProperties(businessTrackVo,tbBusiness);
        tbBusiness.setUpdateTime(new Date());
        tbBusiness.setId(businessTrackVo.getBusinessId());
        tbBusinessMapper.updateTbBusiness(tbBusiness);
        //设置跟进表的属性
        TbBusinessTrackRecord tbBusinessTrackRecord = new TbBusinessTrackRecord();
        BeanUtils.copyProperties(businessTrackVo,tbBusinessTrackRecord);
        tbBusinessTrackRecord.setCreateBy(username);
        tbBusinessTrackRecord.setCreateTime(new Date());
        //正常跟进
        tbBusinessTrackRecordMapper.insert(tbBusinessTrackRecord);
    }
}
