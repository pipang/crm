package com.huike.clues.service;


import com.huike.clues.domain.TbClueTrackRecord;
import com.huike.clues.domain.vo.ClueTrackRecordVo;
import com.huike.clues.domain.vo.FalseClueVo;

import java.util.List;

/**
 * 线索跟进记录Service接口
 *
 * @author WGL
 * @date 2022-04-19
 */
public interface ITbClueTrackRecordService {

    /**
     * 线索跟进表新增数据
     * @param tbClueTrackRecordVo
     */
    void add(ClueTrackRecordVo tbClueTrackRecordVo);

    /**
     * 获取跟进表数据
     * @param clueId
     * @return
     */
    List<TbClueTrackRecord> getClueTrackRecordList(Long clueId);


}
