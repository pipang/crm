package com.huike.clues.service.impl;


import com.huike.clues.domain.TbClue;
import com.huike.clues.domain.TbClueTrackRecord;
import com.huike.clues.domain.vo.ClueTrackRecordVo;
import com.huike.clues.domain.vo.FalseClueVo;
import com.huike.clues.mapper.TbClueMapper;
import com.huike.clues.mapper.TbClueTrackRecordMapper;
import com.huike.clues.service.ITbClueTrackRecordService;
import com.huike.common.utils.SecurityUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * 线索跟进记录Service业务层处理
 * @date 2022-04-22
 */
@Service
public class TbClueTrackRecordServiceImpl implements ITbClueTrackRecordService {

    @Autowired
    private TbClueTrackRecordMapper tbClueTrackRecordMapper;
    @Autowired
    private TbClueMapper tbClueMapper;
    /**
     * 线索跟进表新增数据
     * @param tbClueTrackRecordVo
     */
    @Override
    public void add(ClueTrackRecordVo tbClueTrackRecordVo) {
        // 2.1 由于查询需要用到用户名 调用工具类获取用户名
        String username = SecurityUtils.getUsername();
        //设置线索表的属性
        TbClue tbClue = new TbClue();
        BeanUtils.copyProperties(tbClueTrackRecordVo,tbClue);
        tbClue.setUpdateTime(new Date());
        tbClue.setId(tbClueTrackRecordVo.getClueId());
        tbClueMapper.updateTbClue(tbClue);
        //设置跟进表的属性
        TbClueTrackRecord tbClueTrackRecord = new TbClueTrackRecord();
        BeanUtils.copyProperties(tbClueTrackRecordVo,tbClueTrackRecord);
        tbClueTrackRecord.setCreateBy(username);
        tbClueTrackRecord.setCreateTime(new Date());
        //正常跟进
        tbClueTrackRecord.setType("0");
        tbClueTrackRecordMapper.insert(tbClueTrackRecord);
    }

    /**
     * 查询线索跟进
     * @param clueId
     * @return
     */
    @Override
    public List<TbClueTrackRecord> getClueTrackRecordList(Long clueId) {
        List<TbClueTrackRecord> tbClueTrackRecordList = tbClueTrackRecordMapper.getTbClueTrackRecordList(clueId);
        return tbClueTrackRecordList;
    }

}
