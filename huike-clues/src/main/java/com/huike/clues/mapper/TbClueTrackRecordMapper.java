package com.huike.clues.mapper;


import com.huike.clues.domain.TbClue;
import com.huike.clues.domain.TbClueTrackRecord;
import com.huike.clues.domain.vo.ClueTrackRecordVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 线索跟进记录Mapper接口
 *
 * @date 2021-04-19
 */
public interface TbClueTrackRecordMapper {

    /**
     * 线索更进表新增数据
     * @param tbClueTrackRecord
     */
    void insert(TbClueTrackRecord tbClueTrackRecord);


    /**
     * 获取跟进表数据
     * @param clueId
     * @return
     */
    List<TbClueTrackRecord> getTbClueTrackRecordList(Long clueId);
}
