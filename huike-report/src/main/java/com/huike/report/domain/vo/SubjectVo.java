package com.huike.report.domain.vo;

import lombok.Data;

/**
 * @author zenghao
 * @date 2022/6/12 10:48
 */
@Data
public class SubjectVo {
    private String subject;  //学科
    private Integer num=0;  //学科统计数量
}
