package com.huike.report.mapper;

import java.util.List;
import java.util.Map;

import com.huike.contract.domain.TbContract;
import com.huike.report.domain.vo.SubjectVo;
import org.apache.ibatis.annotations.Param;

import com.huike.clues.domain.vo.IndexStatisticsVo;

/**
 * 首页统计分析的Mapper
 * @author Administrator
 *
 */
public interface ReportMapper {
	/**=========================================基本数据========================================*/
	/**
	 * 获取线索数量
	 * @param beginCreateTime	开始时间
	 * @param endCreateTime		结束时间
	 * @param username			用户名
	 * @return
	 */
	Integer getCluesNum(@Param("startTime") String beginCreateTime,
						@Param("endTime") String endCreateTime,
						@Param("username") String username);

	/**
	 * 获取商机数量
	 * @param beginCreateTime	开始时间
	 * @param endCreateTime		结束时间
	 * @param username			用户名
	 * @return
	 */
	Integer getBusinessNum(@Param("startTime") String beginCreateTime,
						   @Param("endTime") String endCreateTime,
						   @Param("username") String username);

	/**
	 * 获取合同数量
	 * @param beginCreateTime	开始时间
	 * @param endCreateTime		结束时间
	 * @param username			用户名
	 * @return
	 */
	Integer getContractNum(@Param("startTime") String beginCreateTime,
						   @Param("endTime") String endCreateTime,
						   @Param("username") String username);

	/**
	 * 获取合同金额
	 * @param beginCreateTime	开始时间
	 * @param endCreateTime		结束时间
	 * @param username			用户名
	 * @return
	 */
	Double getSalesAmount(@Param("startTime") String beginCreateTime,
						  @Param("endTime") String endCreateTime,
						  @Param("username") String username);

	/**=========================================今日简报========================================*/
	/**
	 * 获取线索数量
	 * @param username			用户名
	 * @return
	 */
	Integer getTodayCluesNum(@Param("username") String username);

	/**
	 * 获取商机数量
	 * @param username			用户名
	 * @return
	 */
	Integer getTodayBusinessNum(@Param("username") String username);

	/**
	 * 获取合同数量
	 * @param username			用户名
	 * @return
	 */
	Integer getTodayContractNum(@Param("username") String username);

	/**
	 * 获取合同金额
	 * @param username			用户名
	 * @return
	 */
	Double getTodaySalesAmount(@Param("username") String username);


	/**=========================================待办========================================*/

	/**
	 * 获取待跟进线索数量
	 * @param beginCreateTime	开始时间
	 * @param endCreateTime		结束时间
	 * @param username			用户名
	 * @return
	 */
	Integer getTodoCluesNum(@Param("startTime") String beginCreateTime,
						@Param("endTime") String endCreateTime,
						@Param("username") String username);


	/**
	 * 获取商机数量
	 * @param beginCreateTime	开始时间
	 * @param endCreateTime		结束时间
	 * @param username			用户名
	 * @return
	 */
	Integer getTodoBusinessNum(@Param("startTime") String beginCreateTime,
						   @Param("endTime") String endCreateTime,
						   @Param("username") String username);

	/**
	 * 统计
	 * @param beginCreateTime
	 * @param endCreateTime
	 * @return
	 */
	List<Map<String,Object>> selectSubjectList(@Param("startTime") String beginCreateTime,
												 @Param("endTime") String endCreateTime);

	/**
	 * 统计
	 * @param beginCreateTime
	 * @param endCreateTime
	 * @return
	 */
	List<Map<String,Object>> getBusiness(@Param("startTime") String beginCreateTime,
											   @Param("endTime") String endCreateTime);

	/**
	 * 统计
	 * @param beginCreateTime
	 * @param endCreateTime
	 * @return
	 */
	List<Map<String,Object>> getClue(@Param("startTime") String beginCreateTime,
											   @Param("endTime") String endCreateTime);
}
